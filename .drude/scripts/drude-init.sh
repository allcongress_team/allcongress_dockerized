#!/bin/bash
set -e

# Set to the appropriate site directory
SITE_DIRECTORY='default'
# Set to the appropriate site directory
SITE_DOMAIN='allcongress.drude'
# Set to the appropriate website URL
LIVE_SITE_DOMAIN='https://asclepius.allcongress.com'
# Set to the appropriate site alias for the DB source
#SOURCE_ALIAS='@none'
SOURCE_DBEXPORT='../drupal_install/DB.sql'

# Console colors
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[1;33m'
NC='\033[0m'

# Get project root directory
GIT_ROOT=$(git rev-parse --show-toplevel)
if [[ -z $GIT_ROOT  ]]; then exit -1; fi

# Set repo root as working directory.
cd $GIT_ROOT

# Check whether shell is interactive (otherwise we are running in a non-interactive script environment)
is_tty ()
{
	[[ "$(/usr/bin/tty || true)" != "not a tty" ]]
}

# Yes/no confirmation dialog with an optional message
# @param $1 confirmation message
_confirm ()
{
	# Skip checks if not a tty
	if ! is_tty ; then return 0; fi
	while true; do
		read -p "Are you ready to deploy a dev environonment ${SITE_DOMAIN}? [y/n]: " answer
		case $answer in
			[Yy]|[Yy][Ee][Ss] )
				break
				;;
			[Nn]|[Nn][Oo] )
				exit 1
				;;
			* )
				echo 'Please answer yes or no.'
		esac
	done
}

# Copy a settings file from $source to $dest
# Skips if the $dest already exists.
_copy_settings_file()
{
  local source=${1}
  local dest=${2}

  if [[ ! -f $dest ]]; then
    echo -e "${green}Copying ${dest}...${NC}"
    cp $source $dest
  else
    echo -e "${green}${dest} already in place${NC} - replacing"
		rm ${dest}
		cp $source $dest
  fi
}

# Copy settings files
init_settings ()
{

  cd $GIT_ROOT


  _copy_settings_file 'docker-compose.dist_dev.yml' 'docker-compose.yml'


	#dsh exec "mkdir docroot"
	echo -e "${green}Do some cleaning - remove everything from docroot/ ..."
	dsh exec "chmod -R 777 docroot/*"
	dsh exec "rm -rf docroot/*"




}

# Install the site
# @param $1 site-name (domain)
site_install ()
{
  cd $GIT_ROOT
  cd docroot



	echo -e "${green}Building drupal installation from makefile ..."

	dsh exec drush -y make ../drupal_install/allcongress.make .

	cd sites/all/modules/
	dsh exec "rm -rf custom"
	echo -e "${green}Cloning custom modules repository ..."
	dsh exec "git clone https://bitbucket.org/allcongress_team/allcongress_modules.git custom"

	cd ../themes/
	dsh exec "rm -rf allcongress_squared"
	echo -e "${green}Cloning theme repository ..."
	dsh exec "git clone https://bitbucket.org/allcongress_team/allcongress_squared.git"

	#dsh exec "mkdir sites/${SITE_DIRECTORY}/files"
	cd ../../../../
	_copy_settings_file "drupal_install/example.settings.local.php" "docroot/sites/${SITE_DIRECTORY}/settings.local.php"
	_copy_settings_file "drupal_install/settings.php" "docroot/sites/${SITE_DIRECTORY}/settings.php"
}

# Create a new DB
# @param $1 DB name
db_create ()
{
  echo -e "${green}Creating DB ${1}...${NC}"
	cd $GIT_ROOT
  local database=${1}
  local mysql_exec='mysql -h $DB_1_PORT_3306_TCP_ADDR --user=root --password=$DB_1_ENV_MYSQL_ROOT_PASSWORD -e ';
  local query="DROP DATABASE IF EXISTS ${database}; CREATE DATABASE ${database}; GRANT ALL ON ${database}.* TO "'$DB_1_ENV_MYSQL_USER'"@'%'"

  dsh exec "${mysql_exec} \"${query}\""
}


# Import database from the source site alias
db_import ()
{
  #_confirm "Do you want to import DB from ${SOURCE_DBEXPORT}?"

  cd $GIT_ROOT
  cd docroot
  dsh mysql-import ${SOURCE_DBEXPORT}
}

# Misc drush commands to bring DB up-to-date
db_updates ()
{
  cd $GIT_ROOT
  echo -e "${green}Applying DB updates...${NC}"
  cd docroot
  set -x

  dsh drush -l ${SITE_DOMAIN} status
  dsh drush -l ${SITE_DOMAIN} updb -y
  dsh drush -l ${SITE_DOMAIN} fr-all -y
  dsh drush -l ${SITE_DOMAIN} cc all
  dsh drush -l ${SITE_DOMAIN} cron -v

  set +x
}

# Local adjustments
local_settings ()
{
  cd $GIT_ROOT
  echo -e "${green}Applying dev-only local settings...${NC}"
  cd docroot
  set -x

  dsh drush -l ${SITE_DOMAIN} en browsersync dblog devel module_filter stage_file_proxy update -y
	dsh drush variable-set stage_file_proxy_origin "${LIVE_SITE_DOMAIN}"


  set +x
}

# Installing npm modules & Compile Sass
compass_compile ()
{
  cd $GIT_ROOT
  echo -e "${green}Installing npm modules & running gulp ...${NC}"
  set -x

  cd docroot/sites/all/themes/allcongress_squared/.npm
  dsh exec npm install
  dsh exec npm install gulp -g

  set +x
}

# Initialize local Behat settings
init_behat ()
{
  cd $GIT_ROOT

  _copy_settings_file 'tests/behat/behat.yml.dist' 'tests/behat/behat.yml'
}

# Run basic Behat validation tests
run_behat ()
{
  cd $GIT_ROOT

  echo -e "${yellow}Launching Behat validation tests...${NC}"
  cd tests/behat
  dsh behat --format=pretty --out=std --format=junit --out=junit features/drush-si-validation.feature
}

# Project initialization steps
dsh reset
init_settings
dsh reset
# Give MySQL some time to start before importing the DB
sleep 5
site_install
#db_create 'drupal'
sleep 5
db_import
sleep 5
db_updates
local_settings
compass_compile
#init_behat
#run_behat

echo -e "${green}All done!${NC}"
echo -e "${green}Add ${SITE_DOMAIN} to your hosts file (/etc/hosts), e.g.:${NC}"
echo -e "192.168.10.10  ${SITE_DOMAIN}"
echo -e "${green}Open http://${SITE_DOMAIN} in your browser to verify the setup.${NC}"

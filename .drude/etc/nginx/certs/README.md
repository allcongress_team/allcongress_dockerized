# Allcongress Dev environment

[Notes.md] (Notes.md) for Allcongress dev's


## Instructions (Mac and Windows) *** first time only setup

**On Windows** you will need a Linux-type shell. Install [Babun](http://babun.github.io/) before proceeding and run all commands in it.  
Instructions were not tested with other shells on Windows.

1. Install `dsh` (Drude Shell)


    ```
    sudo curl -L https://raw.githubusercontent.com/blinkreaction/drude/master/bin/dsh  -o /usr/local/bin/dsh && sudo chmod +x /usr/local/bin/dsh
    ```

2. Create the `<Projects>` directory

    ```
    mkdir Projects
    cd Projects
    ```

3. Install Drude's prerequisites (vagrant, virtualbox, boot2docker-vagrant)

    ```
    dsh install prerequisites
    dsh install boot2docker
    ```

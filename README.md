# Allcongress Dev environment


## Instructions (Mac and Windows) *** first time only setup

**On Windows** you will need a Linux-type shell. Install [Babun](http://babun.github.io/) before proceeding and run all commands in it.  
Instructions were not tested with other shells on Windows.

1. Install `dsh` (Drude Shell)

    ```
    sudo curl -L https://raw.githubusercontent.com/blinkreaction/drude/master/bin/dsh  -o /usr/local/bin/dsh && sudo chmod +x /usr/local/bin/dsh
    ```

2. Create the `Projects` directory at your machine

    ```
    mkdir Projects
    cd Projects
    ```

3. Install Drude's prerequisites (vagrant, virtualbox, boot2docker-vagrant)

    ```
    dsh install prerequisites
    dsh install boot2docker
    ```


You can find more at     https://github.com/blinkreaction/drude/blob/develop/docs/drude-env-setup.md

## Install local dev steps


1. Clone allcongress_dockerized repo into the Projects directory

    ```
    git clone
    https://vgiannoul@bitbucket.org/allcongress_team/allcongress_dockerized.git
    cd allcongress_dockerized
    ```

2. Initial drupal website installation

    ```
    dsh init (on error please run dsh reset && dsh init)
    ```

3. Use drupal cli  

    ```
    dsh bash (after login to cli remember to go to docroot directory for drush to work)
    ```

4. Add `192.168.10.10  allcongress.drude` to your hosts file (thats not neccessary on mac)

5. Point your browser to

    ```
    https://allcongress.drude
    ```

6. Remember to export DB to commit changes at DB

    ```
    dsh bash
    cd docroot
    drush sql-dump > ../drupal_install/DB.sql
    ```


## Run local dev environment

### start & stop application*

   ```
   dsh start
   ```
   and
   ```
   dsh stop
   ```
   or stop all running containers
   ```
   docker stop $(docker ps -a -q)
   ```

   or remove all running containers
   ```
   docker rm $(docker ps -a -q)
   ```

### Reset Vagrant VM when Networking errors

  ```
  cd .. {at projects folder}
  vagrant reload
  ```
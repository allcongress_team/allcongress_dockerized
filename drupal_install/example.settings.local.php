<?php

# Docker DB connection settings.
$databases['default']['default'] = array (
  'database' => getenv('DB_1_ENV_MYSQL_DATABASE'),
  'username' => getenv('DB_1_ENV_MYSQL_USER'),
  'password' => getenv('DB_1_ENV_MYSQL_PASSWORD'),
  'host' => getenv('DB_1_PORT_3306_TCP_ADDR'),
  'driver' => 'mysql',
);

// Set https to on if it should be.
if ( (isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) === "on")
  || (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && strtolower($_SERVER["HTTP_X_FORWARDED_PROTO"]) === "https")
  || (isset($_SERVER["HTTP_HTTPS"]) && strtolower($_SERVER["HTTP_HTTPS"]) === "on")
  ) {
  $_SERVER["HTTPS"] = "on";
}


# Enable error reporting http://drupal.stackexchange.com/questions/7560/how-to-see-the-error-messages-when-i-get-the-white-screen-of-death

error_reporting(-1);  // Have PHP complain about absolutely everything
$conf['error_level'] = 2;  // Show all messages on your screen, 2 = ERROR_REPORTING_DISPLAY_ALL.
ini_set('display_errors', TRUE);  // These lines just give you content on WSOD pages.
ini_set('display_startup_errors', TRUE);

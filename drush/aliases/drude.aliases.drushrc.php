<?php

$aliases['dev'] = array(
  'root' => '/var/www/docroot',
  'uri'  => 'allcongress.drude',
);

$aliases['stage'] = array(
  'root' => '/var/www/docroot',
  'uri'  => 'stage.allcongress.com',
);
